FROM ubuntu:jammy

ARG NS3_VERSION=3.36
ARG DEBIAN_FRONTEND=noninteractive
ARG BUILD_PROFILE=debug

LABEL org.opencontainers.version="v1.0.0"
LABEL org.opencontainers.image.licenses="ISC"
LABEL org.opencontainers.image.title="ns-3 docker container"
LABEL org.opencontainers.image.description="ns-3 base docker container"

WORKDIR /ns3

ENTRYPOINT ["/ns3/entrypoint.sh"]

RUN apt-get update && apt-get install -y \
    g++ \
    cmake \
    python3-dev \
    pkg-config \
    python3 \
    git \
    wget \
    build-essential

RUN wget http://www.nsnam.org/release/ns-allinone-${NS3_VERSION}.tar.bz2 && \
    tar xjf ns-allinone-${NS3_VERSION}.tar.bz2

ENV BUILD_PROFILE=$BUILD_PROFILE
ENV NS3_VERSION=$NS3_VERSION
ENV NS3_ROOT=/ns3/ns-allinone-${NS3_VERSION}/ns-${NS3_VERSION}

COPY entrypoint.sh entrypoint.sh

WORKDIR ns-allinone-${NS3_VERSION}/ns-${NS3_VERSION}

RUN ./ns3 configure  --enable-examples --enable-tests --build-profile=${BUILD_PROFILE} && ./ns3 build
RUN ln -s /ns3/ns-allinone-${NS3_VERSION}/ns-${NS3_VERSION} /ns3/latest
WORKDIR /ns3/latest
# these two labels will change every time the container is built
# put them at the end because of layer caching
ARG VCS_REF
LABEL org.opencontainers.image.revision="${VCS_REF}"

ARG BUILD_DATE
LABEL org.opencontainers.image.created="${BUILD_DATE}"
