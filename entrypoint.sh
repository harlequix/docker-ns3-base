#!/bin/bash

#Use DEBUG to activate debugging logging of the environment
if [[ -n $DEBUG && ( $DEBUG == "true" || $DEBUG == "True" || $DEBUG == "1" ) ]]; then
  export _DEBUG=1
fi

if [[ "$1" == "bash" ]]; then
    bash --init-file <(echo "ls; pwd")
    exit 0
else
    [[ $_DEBUG -eq 1 ]] && echo "ns3 run and pass through command '$@'"
    ./ns3 run "$@"
    exit $?
fi
